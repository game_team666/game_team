﻿using UnityEngine;
using System.Collections;

public class FirstPersonController : MonoBehaviour {

	public float movementSpeed=5.0f;
	public float mouseResponse=5.0f;
	public float UDRange = 60.0f;
	float verticalVelocity=0;
	float jumpspeed=5.0f;
	float verticalrotation = 0;
	bool ii=false;
	//float forwardSpeed = 0 ;
	//float sideSpeed =0;



	CharacterController chacracterController;

	// Use this for initialization
	void Start () {
		Screen.lockCursor = true;
	}
	
	// Update is called once per frame
	void Update () {
		chacracterController = GetComponent<CharacterController>();

		//Rotate
		float rotLR=Input.GetAxis ("Mouse X")* mouseResponse;
		transform.Rotate (0, rotLR, 0);


		verticalrotation -= Input.GetAxis ("Mouse Y")* mouseResponse;
		verticalrotation = Mathf.Clamp (verticalrotation, -UDRange, UDRange);
		Camera.main.transform.localRotation = Quaternion.Euler (verticalrotation, 0, 0);

		//Move


		float forwardSpeed_new = Input.GetAxis ("Vertical")*movementSpeed;
		float sideSpeed_new = Input.GetAxis ("Horizontal")*movementSpeed;



		verticalVelocity += Physics.gravity.y * Time.deltaTime;
		if (chacracterController.isGrounded)
			verticalVelocity = 0;

		//double jump
		if ( Input.GetButtonDown ("Jump")) {
			if(chacracterController.isGrounded && !ii){
				verticalVelocity = jumpspeed;
				ii=true;
			}
			else if(ii==true){
				ii=false;
				verticalVelocity = jumpspeed;
			}
		}


		//Vector3 speed_old = new Vector3 (sideSpeed, 0, forwardSpeed);

		Vector3 speed_new = new Vector3 (sideSpeed_new, verticalVelocity, forwardSpeed_new);

		Vector3 speed_new2 = transform.rotation * speed_new;

		speed_new = transform.rotation * speed_new; //+ speed_old;
		//sideSpeed += speed_new2.x;
		//forwardSpeed += speed_new2.z;

		chacracterController.Move (speed_new*Time.deltaTime);
	}
}

﻿using UnityEngine;
using System.Collections;

public class Rocket_Shoot : MonoBehaviour {

	public float Rocketlaunch = 5f;
	public GameObject rocket_prefab;
	public float Rocketoffset = 5f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Fire1"))
		 {

			Camera cam = Camera.main;
			Vector3 position_Rocket = cam.transform.position;
			Vector3 Rocket_1 = new Vector3(0,0,Rocketoffset);
			Rocket_1 = transform.rotation*Rocket_1;
			position_Rocket += Rocket_1;

			GameObject Rocket = (GameObject)Instantiate(rocket_prefab,position_Rocket,transform.rotation);
			Rocket.rigidbody.AddForce(cam.transform.forward * Rocketlaunch,ForceMode.Impulse);
		}
	}
}

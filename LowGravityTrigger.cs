﻿using UnityEngine;
using System.Collections;


public class LowGravityTrigger : MonoBehaviour {
	public float lowGravity = 10f;

	void OnTriggerStay (Collider other)
	{
		other.rigidbody.AddForce(Vector3.up*lowGravity , ForceMode.Acceleration);
	}
}
